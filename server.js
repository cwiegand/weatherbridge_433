var fs = require('fs'),
    net = require('net'),
    spawn = require('child_process').spawn,
    moment = require('moment'),
    http = require('http'),
    argv = require('minimist')(process.argv.slice(2)),
    path = require('path'),
    querystring = require('querystring');

// ok, basic premise: startup, initialize local state machine, run rtl_433 with options,
// then parse lines we get, and forward to appropriate output(s)

var config = JSON.parse(fs.readFileSync('config.js'));
if (!config.path_to_433) config.path_to_433 = './rtl_433'; // default
if (!config.aprs_pass) config.aprs_pass = '-1';
if (!config.aprs_server) config.aprs_server = 'cwop.aprs.net';
if (argv.debug) config.debug = true;
if (argv.test) config.test = true;
if (config.latitude) { 
  config.latitude.aprs = zeroPad(config.latitude.deg,3) + zeroPad(config.latitude.min,2) + '.' + zeroPad(config.latitude.sec,2) + 'N';
  config.latitude.dec = (config.latitude.deg + (config.latitude.min/60.) + (config.latitude.sec/3600.)).toFixed(4);
}
if (config.longitude) { 
  config.longitude.aprs = zeroPad(config.longitude.deg,3) + zeroPad(config.longitude.min,2) + '.' + zeroPad(config.longitude.sec,2) + 'W';
  config.longitude.dec = (config.longitude.deg + (config.longitude.min/60.) + (config.longitude.sec/3600.)).toFixed(4);
}
if (config.openweathermap_user && config.openweathermap_pass) {
  config.openweathermap_auth = Buffer(config.openweathermap_user + ':' + config.openweathermap_pass).toString('base64');
}
console.log("Config I'm using:");
console.log(JSON.stringify(config));

// state "database"
var state = { };

// following function is from https://stackoverflow.com/questions/1267283/how-can-i-create-a-zerofilled-value-using-javascript
function zeroPad(num, numZeros) {
	var n = Math.abs(num);
	var zeros = Math.max(0, numZeros - Math.floor(n).toString().length);
	var zeroString = Math.pow(10,zeros).toString().substr(1);
	if (num < 0) { zeroString = '-' + zeroString; }
	return zeroString+n;
}

function parse433Line(line) {
  console.log(line);
  if (line.indexOf('wind speed: ') == -1) return; // doesn't match the output, sorry!
  var parts = line.split(',');
  parts = parts.map(function (part) {
    console.log(part);
    if (part.indexOf('wind speed: ') > -1) { state.windSpeed = part.substring('wind speed: '.length).replace(/[^\d.-]/g,''); } 
    if (part.indexOf('wind direction: ') > -1) { state.windDir = part.substring('wind direction: '.length).replace(/[^\d.-]/g,''); } 
    if (part.indexOf('rain gauge: ') > -1) { state.rainGauge = part.substring('rain gauge: '.length).replace(/[^\d.-]/g,''); }
    if (part.indexOf('humidity: ') > -1) { state.humidity = part.substring('humidity: '.length).replace(/[^\d.-]/g,''); } 
    if (part.indexOf('temp: ') > -1) { state.temp = part.substring('temp: '.length).replace(/[^\d.-]/g,''); } 
  });
  console.log('State: ');
  console.log('  Temp: ' + state.temp);
  console.log('  Humidity: ' + state.humidity);
  console.log('  Rain Gauge: ' + state.rainGauge);
  console.log('  Wind Speed: ' + state.windSpeed);
  console.log('  Wind Direction: ' + state.windDir);
  send_wxdata();
  send_wunderground();
  send_openweathermap();
  if (!config.next_aprs_send || config.next_aprs_send <= moment()) {
    setTimeout(send_aprs, 8000);
    config.next_aprs_send = moment().add(5, 'minutes').calendar();
  }
}

function send_wxdata() {
  try {
    var contents = 'temp = ' + state.temp + '\n';
    contents += 'humi = ' + state.humidity + '\n';
    contents += 'wspd = ' + state.windSpeed + '\n';
    contents += 'wdir = ' + state.windDir + '\n';
    fs.writeFileSync('wxdata', contents);
  } catch (e) { console.log(e); }
}

function send_wunderground() {
  if (config.wu_user) { 
    var url = 'http://rtupdate.wunderground.com/weatherstation/updateweatherstation.php?action=updateraw&softwaretype=weather433_bridge&realtime=1&rtfreq=2.5';
    url += "&ID=" + encodeURIComponent(config.wu_user);
    url += "&PASSWORD=" + encodeURIComponent(config.wu_pass);
    url += "&dateutc=" + encodeURIComponent(moment.utc().format().replace("+00:00",""));
    if (state.humidity) url += "&humidity=" + encodeURIComponent(state.humidity);
    if (state.temp) url += "&tempf=" + encodeURIComponent(state.temp);
    if (state.windDir) url += "&winddir=" + encodeURIComponent(state.windDir);
    if (state.windSpeed) url += "&windspeedmph=" + encodeURIComponent(state.windSpeed);
    
    if (!!config.debug)
      console.log(url);
    else {
      try {
        http.get(url)
          .on('data', function (data) { console.log(data); })
          .on('error', function (err) { console.log(err); });
      } catch (e) { console.log(e); }
    }
  } 
}

function send_openweathermap() {
  if (config.openweathermap_user) {
    var data = {};
    data.long = config.longitude.dec;
    data.lat = config.latitude.dec;
    data.alt = config.altitude * 0.3048;
    data.name = "weather_bridge_433";
    if (state.humidity) data.humidity = state.humidity;
    if (state.temp) data.temp = (state.temp - 32) * 5 / 9;
    if (state.windDir)  data.wind_dir = state.windDir;
    if (state.windSpeed) data.wind_speed = state.windSpeed * 0.44704; // convert MPH to M/S

    var dataStr = querystring.stringify(data);
    var options = { 
      host: 'openweathermap.org',
      port: 80,
      path: '/data/post',
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Content-Length': Buffer.byteLength(dataStr),
        'Authorization': 'Basic ' + config.openweathermap_auth
      }
    };
    if (!!config.debug)
      console.log(dataStr + ' ' + JSON.stringify(options));
    else {
      try {
        var req = http.request(options)
          .on('data', function (data) { console.log(data); })
          .on('error', function (err) { console.log(err); });
        req.write(dataStr);
        req.end();
      } catch (e) { console.log(e); }
    }
  }
}

function send_aprs() {
  if (config.aprs_user && config.aprs_lat && config.aprs_lon) {
    var string = config.aprs_user + '>APRS,TCPIP*:!';
    string += config.latitude.aprs + '/' + config.longitude.aprs;
    if (state.windDir && state.windSpeed) { string += '_' + zeroPad(~~state.windDir,3) + '/' + zeroPad(~~state.windSpeed,3); }
    else { string += '_.../...'; }
    if (state.temp) { 
      if (state.temp < -99) { string += "t-99"; }
      else if (state.temp < 0) { string += "t" + zeroPad(~~state.temp,2); }
      else if (state.temp <= 999) { string += "t" + zeroPad(~~state.temp,3); }
      else { string += "t..."; }
    }
    if (state.humidity) { 
      if (state.humidity > 99) { string += "h99"; }
      else if (state.humidity < 1) { string += "h01"; }
      else { string += "h" + zeroPad(~~state.humidity,2); }
    }
    string += "XPIC\n";
    
    if (!!config.debug)
      console.log(string);
    else {
      try {
        var login = "user " + config.aprs_user + " pass " + config.aprs_pass + " vers weatherbridge_433 0.01\n";
        console.log("connecting to " + config.aprs_server + ":14580");
        var socket = net.createConnection(14580, config.aprs_server);
        socket
          .on('error', function (err) { console.log(err); })
          .on('connect', function(connect) { console.log("APRS connected."); socket.write(login); })
          .on('data', function (data) {
              console.log("APRS: " + data);
              if (string) { socket.write(string); console.log("WROTE: " + string); string = null; }
              else { console.log("APRS-CLOSING"); socket.destroy(); } // done!
            }) 
          .on('close', function() { console.log("APRS-CLOSED"); })
          .setTimeout(5000);
      } catch (e) { console.log(e); }
    }
  }
}

var Run433 = {}; // global please
function startService() {
  Run433 = spawn(config.path_to_433,[],{});
  Run433.stderr.on('data', function(data) { 
    var str = data.toString(), lines = str.split(/(\r?\n)/g);
    for (var i=0; i<lines.length; i++) { parse433Line(lines[i]); }
  });
  Run433.stdout.on('data', function(data) { 
    var str = data.toString(), lines = str.split(/(\r?\n)/g);
    for (var i=0; i<lines.length; i++) { parse433Line(lines[i]); }
  });
  Run433.on('close', function(code) {
    console.log('Child process exited with code ' + code);
  });
  console.log("Started " + config.path_to_433);
}

function startTesting() {
  parse433Line("wind speed: 0 kph, temp: 57.0° F, humidity: 74% RH");
  parse433Line("wind speed: 0 kph, wind direction: 0.0°, rain gauge: 0.00 in.");
  setTimeout(function() { console.log("Goodbye!"); }, 5000);
}

if (!!config.test) startTesting();
else startService();

